﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;


            Label2.Visibility = Visibility.Hidden;
            Label1.Visibility = Visibility.Hidden;
            textBox5.Visibility = Visibility.Hidden;
            textBox6.Visibility = Visibility.Hidden;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {



            double a, b, c, D = 0;

            if (double.TryParse(textBox1.Text, out a))
            {
                if (double.TryParse(textBox2.Text, out b))
                {
                    if (double.TryParse(textBox3.Text, out c))
                    {
                        D = Math.Pow(b, 2) - 4 * a * c;
                        textBox4.Text = D.ToString("F2");
                        textBox5.Text = (-1 * b + Math.Sqrt(D)).ToString("F2"); 
                        textBox6.Text = (-1 * b - Math.Sqrt(D)).ToString("F2");
                    }
                    else
                    {
                        MessageBox.Show("Помилка введення значення c!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Помилка введення значення d!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Помилка введення значення a!", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }


            if (D > 0) { Label2.Visibility = Visibility.Visible; textBox6.Visibility = Visibility.Visible; }

            if (D >= 0)
            { 
                Label1.Visibility = Visibility.Visible;
                textBox5.Visibility = Visibility.Visible;
                
            }
            
            if(D < 0)
            {
                MessageBox.Show("Коренів немає", "D < 0", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }


        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_2(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged_3(object sender, TextChangedEventArgs e)
        {

        }
    }
}
